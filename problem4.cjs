function carYears(inventory){
    if(typeof inventory === 'undefined'||inventory === null || inventory.length === 0|| typeof inventory !== 'object'|| (!Array.isArray(inventory))){
        return [];
    }
    else{
        let carArray = new Array();
        for(let index=0;index<inventory.length;index++){
            carArray[index] = inventory[index].car_year;
        }
        return carArray;
    }
    
}
module.exports = carYears;