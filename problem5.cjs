function olderCars(inventory){
    if(typeof inventory === 'undefined'||inventory === null || inventory.length === 0|| typeof inventory !== 'object'|| (!Array.isArray(inventory))){
        return [];
    }
    else{
        let cars=0;
        for(let index=0;index<inventory.length;index++){
            if(inventory[index].car_year <2000){
                cars++;
            }
        }
        return cars;
    }
    
}
module.exports = olderCars;