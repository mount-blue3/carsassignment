
function findCar(inventory,id){
    if(typeof inventory === 'undefined'||inventory === null || inventory.length === 0 || typeof id !== 'number' || id === 'undefined' || typeof inventory !== 'object'|| (!Array.isArray(inventory))){
        return [];
    }
    else{
        for(let index=0;index<inventory.length;index++){
            if(inventory[index].id === id){
                return inventory[index];
            }
            
        }
    }
    
    
} 
module.exports = findCar;

