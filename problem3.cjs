function sortedCarModels(inventory){
    if(typeof inventory === 'undefined'||inventory === null || inventory.length === 0|| typeof inventory !== 'object'|| (!Array.isArray(inventory))){
        return [];
    }
    else{
        let carModel = new Array();
        for(let index=0;index<inventory.length;index++){

            carModel[index] = inventory[index].car_model;
        }
        let sc = new Array();
        sc = carModel.sort();
        return sc;
    }
    
}
module.exports = sortedCarModels;