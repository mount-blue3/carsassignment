function carArray(inventory){
    if(typeof inventory === 'undefined'||inventory === null || inventory.length === 0|| typeof inventory !== 'object'|| (!Array.isArray(inventory))){
        return [];
    }
    else{
        let array1 = new Array();
        for(let index=0;index<inventory.length;index++){
            if(inventory[index].car_make==='Audi'|| inventory[index].car_make==='BMW'){
                array1.push(inventory[index]);
            }
        }
        return array1;
    }
    
}
module.exports = carArray;