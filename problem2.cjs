function findLastCar(inventory){
    if(typeof inventory === 'undefined'||inventory === null || inventory.length === 0|| typeof inventory !== 'object'|| (!Array.isArray(inventory))){
        return [];
    }
    else{
        let l = inventory.length;
        return inventory[l-1];
    }
    
}
module.exports = findLastCar;